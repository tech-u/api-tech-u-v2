package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.sound.sampled.Port;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @RequestMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.productService.findAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es:"+id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

        // (condicion) ? vale_esto_si_true : vale_esto_si_false
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel productModel, @PathVariable String id){
        System.out.println("updateProduct");

        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if(productToUpdate.isPresent()){
            System.out.println("Producto para actualizar encontrador, actualizando");

            this.productService.update(productModel);
        }

        return new ResponseEntity<>(
                productModel, productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct (@PathVariable String id) {
        System.out.println("deleteProduct");

        boolean deleteProduct = this.productService.delete(id);

        return new ResponseEntity<>(
            deleteProduct ? "Producto eliminado" : "Producto no borrado",
            deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct");
        System.out.println("La id del producto a crear es: " + product.getId());

        return new ResponseEntity<>(
                this.productService.add(product),
                HttpStatus.CREATED
        );
    }
}