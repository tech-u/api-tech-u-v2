package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;
    @Autowired
    UserService userService;
    @Autowired
    ProductService productService;

    @RequestMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases(){

        return new ResponseEntity<>(
                this.purchaseService.findAll(),
                HttpStatus.OK
        );
    }

    /*
    @PostMapping("/purchases")
    public ResponseEntity<PurchaseServiceResponse> addPurchase(@RequestBody PurchaseModel purchase) {
        PurchaseServiceResponse result = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(result, result.getResponseHttpStatusCode());
    } */


    @PostMapping("/purchases")
    public ResponseEntity<String> addPurchase(@RequestBody PurchaseModel purchase){

        Optional<UserModel> userToValidate = this.userService.findById(purchase.getUserId());
        Optional<PurchaseModel> purchaseToValidate = this.purchaseService.findById(purchase.getId());


        if(userToValidate.isPresent()){
            if(!purchaseToValidate.isPresent()){
                this.purchaseService.add(purchase);

                return new ResponseEntity<>(
                        "Compra validada y creada correctamente.",
                        HttpStatus.CREATED
                );
            } else {
                return new ResponseEntity<>(
                        !purchaseToValidate.isPresent() ? "Id de compra validada." : "Error: Id de compra NO validada.",
                        !purchaseToValidate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
                );
            }
        } else {
            return new ResponseEntity<>(
                    userToValidate.isPresent() ? "Id de usuario validada." : "Error: Id de usuario NO validada.",
                    userToValidate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
            );
        }
    }
}
