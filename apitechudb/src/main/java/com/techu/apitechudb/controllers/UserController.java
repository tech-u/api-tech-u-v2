package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
//@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(name = "$orderby", required = false) String orderBy){
        System.out.println("getUsers");

        return new ResponseEntity<>(
                this.userService.getUsers(orderBy),
                HttpStatus.OK
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La id del user a crear es: " + user.getId());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es:"+id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

        // (condicion) ? vale_esto_si_true : vale_esto_si_false
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser (@PathVariable String id) {
        System.out.println("deleteUser");

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario eliminado" : "Usuario no borrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel userModel, @PathVariable String id){
        System.out.println("updateProduct");

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if(userToUpdate.isPresent()){
            System.out.println("Usuario para actualizar encontrador, actualizando");

            this.userService.update(userModel);
        }

        return new ResponseEntity<>(
                userModel, userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
