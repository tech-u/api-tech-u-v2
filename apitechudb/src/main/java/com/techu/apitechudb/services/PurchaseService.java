package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;
    @Autowired
    ProductService productService;
    @Autowired
    UserService userService;

    public PurchaseModel add(PurchaseModel purchase) {
        return this.purchaseRepository.save(purchase);
    }

    /*public PurchaseServiceResponse addPurchase(PurchaseModel purchase) {
        PurchaseServiceResponse result = new PurchaseServiceResponse();

        result.setPurchase(purchase);

        if(this.userService.findById(purchase.getUserId()).isEmpty() == true){
            result.setMsg("El usuario de la compra no se ha encontrado.");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        if(this.purchaseRepository.findById(purchase.getId()).isPresent() == true){
            result.setMsg("Ya hay una compra con esa id.");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        float amount = 0;

        for(Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()){
            if(this.productService.findById(purchaseItem.getKey()).isEmpty()){
                result.setMsg("El producto con la id " + purchaseItem.getKey()+ " no se encuentra en el sistema");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
                return result;
            } else {
                amount += (this.productService.findById(purchaseItem.getKey()).get().getPrice() * purchaseItem.getValue());
            }
        }

        purchase.setAmount(amount);
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.CREATED);
        return result;
    }*/

    public List<PurchaseModel> findAll() {
        return this.purchaseRepository.findAll();
    }

    public Optional<PurchaseModel> findById(String id){
        return this.purchaseRepository.findById(id);
    }
}
